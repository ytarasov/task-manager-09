package ru.t1.ytarasov.tm.repository;

import ru.t1.ytarasov.tm.api.ICommandRepository;
import ru.t1.ytarasov.tm.constant.ArgumentConst;
import ru.t1.ytarasov.tm.constant.TerminalConst;
import ru.t1.ytarasov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );
    public static Command EXIT = new Command(
            TerminalConst.EXIT, null, "Exit from application."
    );
    public static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Display system info."
    );
    public static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );
    public static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, EXIT, HELP, INFO, VERSION
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
