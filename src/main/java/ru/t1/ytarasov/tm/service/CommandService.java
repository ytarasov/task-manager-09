package ru.t1.ytarasov.tm.service;

import ru.t1.ytarasov.tm.api.ICommandRepository;
import ru.t1.ytarasov.tm.api.ICommandService;
import ru.t1.ytarasov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
