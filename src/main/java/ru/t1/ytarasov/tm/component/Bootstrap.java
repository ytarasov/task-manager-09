package ru.t1.ytarasov.tm.component;

import ru.t1.ytarasov.tm.api.ICommandController;
import ru.t1.ytarasov.tm.api.ICommandRepository;
import ru.t1.ytarasov.tm.api.ICommandService;
import ru.t1.ytarasov.tm.constant.ArgumentConst;
import ru.t1.ytarasov.tm.constant.TerminalConst;
import ru.t1.ytarasov.tm.controller.CommandController;
import ru.t1.ytarasov.tm.repository.CommandRepository;
import ru.t1.ytarasov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void runArguments(String[] args) {
        if (args == null || args.length == 0) {
            runCommands();
            return;
        }
        final String arg = args[0];
        runArgument(arg);
    }

    private void runArgument(final String arg) {
        commandController.displayWelcome();
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            default:
                commandController.displayError();
        }
    }

    private void runCommands() {
        commandController.displayWelcome();
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (true) {
            command = scanner.nextLine();
            runCommand(command);
        }
    }

    private void runCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.EXIT:
                exitApplication();
            default:
                commandController.displayError();
        }
    }

    public void exitApplication() {
        System.exit(0);
    }

}
